# Parasoft dotTEST Integration for GitLab

This project provides example pipelines that demonstrate how to integrate Parasoft dotTEST with GitLab. The integration enables you to run code analysis with Parasoft dotTEST and review analysis results directly in GitLab.

Parasoft dotTEST is a testing tool that automates software quality practices for C# and VB.NET applications. It uses a comprehensive set of analysis techniques, including pattern-based static analysis, dataflow analysis, metrics, code coverage, and unit testing to help you verify code quality and ensure compliance with industry standards, such as CWE or OWASP.

- Request [a free trial](https://www.parasoft.com/products/parasoft-dottest/dottest-request-a-demo/) to receive access to Parasoft dotTEST's features and capabilities.
- See the [user guide](https://docs.parasoft.com/display/DOTTEST20231) for information about Parasoft dotTEST's capabilities and usage.

Please visit the [official Parasoft website](http://www.parasoft.com) for more information about Parasoft dotTEST and other Parasoft products.

- [Quick start](#quick-start)
- [Example Pipelines](#example-pipelines)
- [Reviewing Analysis Results](#reviewing-analysis-results)

## Quick start

To analyze your code with Parasoft dotTEST and review analysis results in GitLab, you need to customize your pipeline to include a job that will:
* run dotTEST.
* upload the analysis report in the SAST format.
* upload the dotTEST analysis reports in other formats (XML, HTML, etc.).


### Prerequisites

* This extension requires Parasoft dotTEST 2021.2 (or newer) with a valid Parasoft license. Starting with GitLab v.16.0.0 and later only SAST report v.15 is accepted. SAST report v.15 is supported in dotTEST v.2023.1.1 and later.
* We recommend that you execute the pipeline on a GitLab runner with the following components installed and configured on the runner:
   - Visual Studio or Build Tools for Visual Studio to build your project
   - Parasoft dotTEST 2021.2 (or newer)

## Example Pipelines

The following example shows a simple pipeline. The example assumes that dotTEST is run on a GitLab runner and the path to the `dottestcli` executable is available on `PATH`.

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/dottest-gitlab/-/blob/main/pipelines/.gitlab-ci.yml) file.

```yaml
# This is a basic pipeline to help you get started with dotTEST integration to analyze a GitLab project.

stages:        
  - test

# Runs code analysis with dotTEST.
dottest-sast:
  stage: test
  script:
    # Configures advanced reporting options and SCM integration.
    - $SCM_SETTINGS="-property report.format=xml,html,sast-gitlab "
    - $SCM_SETTINGS+="-property report.scontrol=min "
    - $SCM_SETTINGS+="-property scontrol.rep.type=git "
    - $SCM_SETTINGS+="-property scontrol.rep.git.url=$CI_PROJECT_URL "
    - $SCM_SETTINGS+="-property scontrol.rep.git.workspace=$CI_PROJECT_DIR "
    - $SCM_SETTINGS+="-property scontrol.rep.git.branch=$CI_COMMIT_BRANCH"
    - $DT_ARGS=$SCM_SETTINGS.Split(" ")
	
    # Launches dotTEST.
    - echo "Running dotTEST..."
    - dottestcli -solution "%CI_PROJECT_DIR%/**/*.sln" -config "builtin://Recommended .NET Core Rules" -settings report.properties -report reports $DT_ARGS
  
  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*

```

## Limiting the Scope of Analysis

If you want to limit the scope of analysis to only see the violations from the current branch, set the `GIT_DEPTH` parameter to 0 before the script to enable an unshallow clone of the repository. For details, see [Shallow cloning](https://docs.gitlab.com/ee/ci/large_repositories/#shallow-cloning).

```yaml
stage: test
  variables:
    GIT_DEPTH: 0
  script:
  ...
```

## Reviewing Analysis Results
When the pipeline completes, you can review the violations reported by dotTEST as code vulnerabilities:
* in the *Security* tab of the GitLab pipeline.
* on GitLab's Vulnerability Report.

You can click each violation reported by dotTEST to review the details and navigate to the code that triggered the violation.

## Baselining Static Analysis Results in Merge Requests
In GitLab, when a merge request is created, static analysis results generated for the branch to be merged are compared with the results generated for the integration branch. As a result, only new violations are presented in the merge request view, allowing developers to focus on the relevant problems for their code changes. 

### Defining a Merge Request Policy
You can define a merge request policy for your integration branch that will block merge requests due to new violations. To configure this:
1. In your GitLab project view, go to **Security & Compliance>Policies**, and select **New policy**.
1. Select the **Scan result policy** type.
1. In the **Policy details** section, define a rule for Static Application Security Testing (select “IF SAST…”). Configure additional options, if needed.

---
## About
dotTEST integration for GitLab - Copyright (C) 2023 Parasoft Corporation
